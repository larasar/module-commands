# Larasar Module Commands
A set of laravel commands for operating on modules with the Larasar framework

## Introduction

This package adds laravel commands to manipulate modules within the Larasar framework.

## Commands

> All commands are module based. This means that a module must be selected for them to work.

> Use the `-h` or `--help` flag to get more details about any of them.

- `php artisan module:create`: Creates a new module within the framework.
- `php artisan module:select`: Select the module to work with.
- `php artisan module:current`: See the currently selected module.
- `php artisan module:status`: Checks the status of a module.
- `php artisan module:publish`: Publishes a module to the module directory.

Other commands work exactly like their `php artisan make:*` counterparts:

- `php artisan module:cast`
- `php artisan module:channel`
- `php artisan module:command`
- `php artisan module:controller`
- `php artisan module:event`
- `php artisan module:exception`
- `php artisan module:factory`
- `php artisan module:job`
- `php artisan module:listener`
- `php artisan module:middleware`
- `php artisan module:migration`
- `php artisan module:migrate`
- `php artisan module:model`
- `php artisan module:notification`
- `php artisan module:observer`
- `php artisan module:policy`
- `php artisan module:provider`
- `php artisan module:request`
- `php artisan module:resource`
- `php artisan module:rule`
- `php artisan module:seeder`
- `php artisan module:seed`
- `php artisan module:test`

## Helper functions

- `module_disk()` - The same as `Storage::disk('module')`. Provides a filesystem disk to the module directory.
- `larasar_module_path(?string $path = null)` - The same as `Storage:disk('module')->path($path)`. Returns the full system module path to the given path.
- `larasar_get_enabled_modules()` - Returns an array of enabled modules.
- `larasar_each_enabled_module(callable $callable)` - Executes the given callable function on each of the enabled modules.
- `larasar_each_enabled_backend_module(callable $callable)` - Executes the given callable function on each of the enabled modules that has a backend.
- `larasar_each_enabled_frontend_module(callable $callable)` - Executes the given callable function on each of the enabled modules that has a frontend.
- `larasar_enable_module_routes()` - Enables the routes provided in each enabled module's `apiRoutes.php`.