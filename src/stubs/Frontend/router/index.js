import MainLayout from 'layouts/MainLayout'
import InfoPage from '../pages/Info'

// import { chainMiddlewares, getMiddleware } from 'src/router/middlewares'
// const guestOnly = getMiddleware('auth.guestOnly')

export default {
    path: '/info',
    component: MainLayout,
    children: [
        {
            path: '',
            name: 'info',
            component: InfoPage
            // , beforeEnter: chainMiddleware([guestOnly])
        }
    ]
}
