<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

/**
 * Provides a filesystem disk to the module directory.
 */
function module_disk() {
  return Storage::disk('module');
}

/**
 * Returns the full system module path to the given path.
 *
 * @param ?string $path An optional path to the target module file/directory
 *
 * @return string The full system module path
 */
function larasar_module_path(?string $path = null): string
{
  return module_disk()->path($path);
}

/**
 * Returns an array of enabled modules.
 */
function larasar_get_enabled_modules(): array
{
  $enabledModulesJson = module_disk()->get('enabled.json');

  return json_decode($enabledModulesJson, $associative = true);
}

/**
 * Executes the given callable function on each of the enabled modules
 */
function larasar_each_enabled_module(callable $callable): void
{
  $enabledModulesArray = larasar_get_enabled_modules();

  foreach ($enabledModulesArray as $moduleName) {
    $result = call_user_func($callable, $moduleName);

    if ($result === false) {
      break;
    }
  }
}

/**
 * Executes the given callable function on each of the enabled modules that has a backend
 */
function larasar_each_enabled_backend_module(callable $callable): void
{
  larasar_each_enabled_module(function ($moduleName) use ($callable) {
    if (!module_disk()->exists("{$moduleName}/Backend")) {
      return;
    }

    return call_user_func($callable, $moduleName);
  });
}

/**
 * Executes the given callable function on each of the enabled modules that has a frontend
 */
function larasar_each_enabled_frontend_module(callable $callable): void
{
  larasar_each_enabled_module(function ($moduleName) use ($callable) {
    if (!module_disk()->exists("{$moduleName}/Frontend")) {
      return;
    }

    return call_user_func($callable, $moduleName);
  });
}

/**
 * Enables the routes provided in each enabled module's `apiRoutes.php`
 */
function larasar_enable_module_routes()
{
  larasar_each_enabled_module(
    fn ($moduleName) => Route::prefix(Str::kebab($moduleName))
      ->group(function () use ($moduleName) {
        $backendPath = "{$moduleName}/Backend";

        if (!module_disk()->exists($backendPath)) {
          return;
        }

        include_once larasar_module_path("{$backendPath}/apiRoutes.php");
      })
  );
}
