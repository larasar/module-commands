<?php

namespace Larasar\Module\Commands;

use Illuminate\Console\Command;
use Larasar\Module\Commands\Traits\Module;

class Status extends Command
{
  use Module;

  protected $signature = 'module:status
                      { name : The name of the module. Example is larasar/auth-module }
                      { --e|enable : Enables the module }
                      { --d|disable : Disables the module }';

  protected $description = 'Checks the status of a module';

  public function handle()
  {
    $name = $this->argument('name');
    $enable = $this->option('enable');
    $disable = $this->option('disable');

    if (!$this->disk()->exists($name)) {
      $this->error("{$name} has not been published. Run \"php artisan module:publish {$name}\"");

      return 1;
    }

    $enabledModules = larasar_get_enabled_modules();

    if (in_array($name, $enabledModules)) {
      if ($disable) {
        $index = array_search($name, $enabledModules);

        unset($enabledModules[$index]);

        $this->saveEnabledModules($enabledModules);
        $this->info("Disabled module {$name}");
      } else {
        $this->info("Module {$name} is enabled");
      }
    } else {
      if ($enable) {
        $enabledModules[] = $name;

        $this->saveEnabledModules($enabledModules);
        $this->info("Enabled module {$name}");
      } else {
        $this->info("Module {$name} is disabled");
      }
    }

    return 0;
  }

  private function saveEnabledModules(array $enabledModules)
  {
    $this->disk()->put("enabled.json", json_encode($enabledModules));
  }
}
