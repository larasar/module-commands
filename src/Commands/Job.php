<?php

namespace Larasar\Module\Commands;

use Illuminate\Foundation\Console\JobMakeCommand;
use Larasar\Module\Commands\Traits\Module;

class Job extends JobMakeCommand
{
  use Module;

  protected $name = 'module:job';
}
