<?php

namespace Larasar\Module\Commands;

use Larasar\Module\Commands\Traits\Module;
use Illuminate\Database\Console\Seeds\SeederMakeCommand;
use Illuminate\Support\Str;

class Seeder extends SeederMakeCommand
{
  use Module;

  protected $name = 'module:seeder';

  protected function getPath($name)
  {
    $name = (string) Str::of($name)->replaceFirst($this->rootNamespace(), '')->finish('Seeder');

    return $this->getRootPath('/Database/Seeders/' . str_replace('\\', '/', $name) . '.php');
  }

  protected function getNamespace($name)
  {
    return $this->rootNamespace() . 'Database\\Seeders';
  }
}
