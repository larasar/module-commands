<?php

namespace Larasar\Module\Commands\Traits;

use Exception;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\FilesystemAdapter;

trait Module
{
  private $disk;
  private $selectedModule;

  public function handle()
  {
    try {
      $this->checkModuleIsSelected();
    } catch (Exception $ex) {
      $this->error($ex->getMessage());

      return 0;
    }

    $this->alert("Module: {$this->selectedModule()}");

    return parent::handle();
  }

  protected function checkModuleIsSelected(): void
  {
    if (empty($this->selectedModule())) {
      throw new Exception('No module selected');
    }

    if (!$this->disk()->exists($this->selectedModule())) {
      throw new Exception('Selected module does not exist');
    }
  }

  protected function disk(): FilesystemAdapter
  {
    if (!$this->disk) {
      $this->disk = Storage::disk('module');
    }

    return $this->disk;
  }

  protected function getCacheKey(): string
  {
    return 'active-module';
  }

  protected function getPath($name)
  {
    return lcfirst(str_replace('\\', '/', $name)) . '.php';
  }

  protected function getRootPath(string $path = '')
  {
    return larasar_module_path($this->selectedModule() . '/Backend' . $path);
  }

  protected function rootNamespace()
  {
    return 'Module\\' . $this->selectedModule() . '\\Backend\\';
  }

  protected function selectedModule(): ?string
  {
    if (empty($this->selectedModule)) {
      $this->selectedModule = Cache::get($this->getCacheKey());
    }

    return $this->selectedModule;
  }
}
