<?php

namespace Larasar\Module\Commands;

use Illuminate\Foundation\Console\EventMakeCommand;
use Larasar\Module\Commands\Traits\Module;

class Event extends EventMakeCommand
{
  use Module;

  protected $name = 'module:event';
}
