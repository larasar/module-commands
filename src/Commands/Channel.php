<?php

namespace Larasar\Module\Commands;

use Illuminate\Foundation\Console\ChannelMakeCommand;
use Larasar\Module\Commands\Traits\Module;

class Channel extends ChannelMakeCommand
{
  use Module;

  protected $name = 'module:channel';
}
