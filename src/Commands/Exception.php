<?php

namespace Larasar\Module\Commands;

use Illuminate\Foundation\Console\ExceptionMakeCommand;
use Larasar\Module\Commands\Traits\Module;

class Exception extends ExceptionMakeCommand
{
  use Module;

  protected $name = 'module:exception';
}
