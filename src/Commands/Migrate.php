<?php

namespace Larasar\Module\Commands;

use Larasar\Module\Commands\Traits\Module;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class Migrate extends Command
{
  use Module;

  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'module:migrate
                { module? : The target module }
                {--database= : The database connection to use}
                {--force : Force the operation to run when in production}
                {--rollback : Rolls back migration}
                {--schema-path= : The path to a schema dump file}
                {--pretend : Dump the SQL queries that would be run}
                {--seed : Indicates if the seed task should be re-run}
                {--step : Force the migrations to be run so they can be rolled back individually}';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Run the database migrations';

  /**
   * Execute the console command.
   *
   * @return int
   */
  public function handle()
  {
    $options = Arr::only($this->options(), $this->getNecessaryOptions());
    $activeOptions = array_filter($options);

    $flagKeys = array_map(fn ($value) => "--{$value}", array_keys($activeOptions));
    $migrateOptions = array_combine($flagKeys, array_values($activeOptions));
    $migrateOptions['--realpath'] = true;

    $command = 'migrate';

    if ($this->option('rollback')) {
      $command .= ':rollback';
    }

    $migrate = function ($module) use ($command, $migrateOptions) {
      $migrateOptions['--path'] = larasar_module_path($module . '/Backend/Database/migrations');

      $this->call($command, $migrateOptions);
    };

    if ($module = $this->argument('module')) {
      $migrate($module);
    } else {
      larasar_each_enabled_backend_module($migrate);
    }

    return 0;
  }

  private function getNecessaryOptions(): array
  {
    $common = ['database', 'force', 'pretend', 'step'];

    return $this->option('rollback') ? $common : array_merge($common, ['schema-path', 'seed']);
  }
}
