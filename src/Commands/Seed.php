<?php

namespace Larasar\Module\Commands;

use Illuminate\Database\Console\Seeds\SeedCommand;
use Larasar\Module\Commands\Traits\Module;
use Symfony\Component\Console\Input\InputOption;

class Seed extends SeedCommand
{
  use Module;

  protected $name = 'module:seed';

  protected function getSeeder()
  {
    $class = $this->input->getArgument('class') ?? $this->input->getOption('class');

    if (strpos($class, '\\') === false) {
      $class = $this->rootNamespace() . 'Database\\Seeders\\' . $class;
    }

    return $this->laravel->make($class)
      ->setContainer($this->laravel)
      ->setCommand($this);
  }

  protected function getOptions()
  {
    return [
      ['class', null, InputOption::VALUE_OPTIONAL, 'The class name of the root seeder', $this->rootNamespace() . 'Database\\Seeders\\DatabaseSeeder'],
      ['database', null, InputOption::VALUE_OPTIONAL, 'The database connection to seed'],
      ['force', null, InputOption::VALUE_NONE, 'Force the operation to run when in production'],
    ];
  }
}
