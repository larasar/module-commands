<?php

namespace Larasar\Module\Commands;

use Illuminate\Foundation\Console\RequestMakeCommand;
use Larasar\Module\Commands\Traits\Module;

class Request extends RequestMakeCommand
{
  use Module;

  protected $name = 'module:request';
}
