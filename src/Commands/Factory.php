<?php

namespace Larasar\Module\Commands;

use Larasar\Module\Commands\Traits\Module;
use Illuminate\Support\Str;
use Illuminate\Database\Console\Factories\FactoryMakeCommand;

class Factory extends FactoryMakeCommand
{
  use Module;

  protected $name = 'module:factory';

  protected function getPath($name)
  {
    $name = (string) Str::of($name)->replaceFirst($this->rootNamespace(), '')->finish('Factory');

    return $this->getRootPath('/Database/Factories/' . str_replace('\\', '/', $name) . '.php');
  }

  protected function buildClass($name)
  {
    $factory = class_basename(Str::ucfirst(str_replace('Factory', '', $name)));

    $namespaceModel = $this->option('model')
      ? $this->qualifyModel($this->option('model'))
      : $this->qualifyModel($this->guessModelName($name));

    $model = class_basename($namespaceModel);

    $namespace = $this->rootNamespace() . 'Database\\Factories';

    if (Str::startsWith($namespaceModel, $this->rootNamespace() . 'Models')) {
      $namespace = Str::beforeLast($namespace . '\\' . Str::after($namespaceModel, $this->rootNamespace() . 'Models\\'), '\\');
    }

    $replace = [
      '{{ factoryNamespace }}' => $namespace,
      'NamespacedDummyModel' => $namespaceModel,
      '{{ namespacedModel }}' => $namespaceModel,
      '{{namespacedModel}}' => $namespaceModel,
      'DummyModel' => $model,
      '{{ model }}' => $model,
      '{{model}}' => $model,
      '{{ factory }}' => $factory,
      '{{factory}}' => $factory,
    ];

    $stub = $this->files->get($this->getStub());

    return str_replace(
      array_keys($replace),
      array_values($replace),
      $this->replaceNamespace($stub, $name)->replaceClass($stub, $name),
    );
  }
}
