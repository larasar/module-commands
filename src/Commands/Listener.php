<?php

namespace Larasar\Module\Commands;

use Illuminate\Foundation\Console\ListenerMakeCommand;
use Larasar\Module\Commands\Traits\Module;

class Listener extends ListenerMakeCommand
{
  use Module;

  protected $name = 'module:listener';
}
