<?php

namespace Larasar\Module\Commands;

use Illuminate\Foundation\Console\ResourceMakeCommand;
use Larasar\Module\Commands\Traits\Module;

class Resource extends ResourceMakeCommand
{
  use Module;

  protected $name = 'module:resource';
}
