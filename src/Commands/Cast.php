<?php

namespace Larasar\Module\Commands;

use Illuminate\Foundation\Console\CastMakeCommand;
use Larasar\Module\Commands\Traits\Module;

class Cast extends CastMakeCommand
{
  use Module;

  protected $name = 'module:cast';
}
