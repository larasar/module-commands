<?php

namespace Larasar\Module\Commands;

use Larasar\Module\Commands\Traits\Module;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class Create extends Command
{
  use Module;

  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'module:create
                        { name : The name of the module }
                        { --backend : Create only the backend structure in the module }
                        { --frontend : Create only the frontend structure in the module }
                        { --force : Force overwriting an existing structure }';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Creates the module directory and subdirectories';

  /**
   * Execute the console command.
   *
   * @return int
   */
  public function handle()
  {
    $name = $this->argument('name');
    $studlyName = Str::studly($name);

    $frontend = $this->option('frontend');
    $backend = $this->option('backend');

    $composePath = "{$studlyName}/docker-compose.yml";
    $gitignorePath = "{$studlyName}/.gitignore";
    $readmePath = "{$studlyName}/README.md";

    if (!$frontend) {
      $this->createBackendDirectories($studlyName);

      if ($backend && $this->canWriteTo($composePath)) {
        File::copy(__DIR__ . '/../stubs/docker-compose.backend.yml', larasar_module_path($composePath));
      }
    }

    if (!$backend) {
      $this->createFrontendDirectories($studlyName);

      if ($frontend && $this->canWriteTo($composePath)) {
        File::copy(__DIR__ . '/../stubs/docker-compose.frontend.yml', larasar_module_path($composePath));
      }
    }

    if ((($frontend && $backend) || (!$frontend && !$backend)) && $this->canWriteTo($composePath)) {
      File::copy(__DIR__ . '/../stubs/docker-compose.yml', larasar_module_path($composePath));
    }

    if ($this->canWriteTo($gitignorePath)) {
      File::copy(__DIR__ . '/../stubs/.gitignore', larasar_module_path($gitignorePath));
    }

    if ($this->canWriteTo($readmePath)) {
      $this->copyFileAndReplaceStubPlaceholder('README.md', $studlyName, $name);
    }

    return 0;
  }

  private function createBackendDirectories(string $moduleName)
  {
    $path = "{$moduleName}/Backend";

    if (!$this->canWriteTo($path)) {
      $this->error($path . ' already exists. Use --force to overwrite the directory.');

      return;
    }

    $this->info("Creating backend directories");

    File::copyDirectory(__DIR__ . '/../stubs/Backend', larasar_module_path($path));

    $this->copyFileAndReplaceStubPlaceholder('composer.json', $moduleName);
    $this->copyFileAndReplaceStubPlaceholder('Backend/apiRoutes.php', $moduleName, $moduleName);
    $this->copyFileAndReplaceStubPlaceholder('Backend/authPolicies.php', $moduleName, $moduleName);
  }

  private function createFrontendDirectories(string $moduleName)
  {
    $path = "{$moduleName}/Frontend";

    if (!$this->canWriteTo($path)) {
      $this->error($path . ' already exists. Use --force to overwrite the directory.');

      return;
    }

    $this->info('Creating frontend directories');

    File::copyDirectory(__DIR__ . '/../stubs/Frontend', larasar_module_path($path));

    $this->copyFileAndReplaceStubPlaceholder('package.json', $moduleName);
    $this->copyFileAndReplaceStubPlaceholder('Frontend/store/index.js', $moduleName);
    $this->copyFileAndReplaceStubPlaceholder('Frontend/pages/Info.vue', $moduleName, $moduleName);
  }

  private function copyFileAndReplaceStubPlaceholder($filename, $moduleName, $stubReplacement = null)
  {
    $content = File::get(__DIR__ . "/../stubs/{$filename}");
    $cleanedContent = str_replace('stub', $stubReplacement ?? Str::kebab($moduleName), $content);

    $this->disk()->put("{$moduleName}/{$filename}", $cleanedContent);
  }

  private function canWriteTo($path)
  {
    return !$this->disk()->exists($path) || $this->option('force');
  }
}
