<?php

namespace Larasar\Module\Commands;

use Larasar\Module\Commands\Traits\Module;
use Illuminate\Console\Command;

class Current extends Command
{
  use Module;

  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'module:current';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Shows the currently selected/active module';

  /**
   * Execute the console command.
   *
   * @return int
   */
  public function handle()
  {
    if (!empty($this->selectedModule())) {
      $this->alert(sprintf('%s module selected', $this->selectedModule));
    } else {
      $this->error('No module selected');
    }

    return 0;
  }
}
