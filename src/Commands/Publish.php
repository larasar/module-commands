<?php

namespace Larasar\Module\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Larasar\Module\Commands\Traits\Module;

class Publish extends Command
{
  use Module;

  protected $signature = 'module:publish
                      { name : The name of the module. Example is larasar/auth-module }
                      { --force : Overwrites the existing module directory }';

  protected $description = 'Publishes a module to the module directory';

  public function handle()
  {
    $vendorName = $this->argument('name');
    $vendorPath = base_path("vendor/{$vendorName}");

    if (!File::exists($vendorPath)) {
      $this->error("{$vendorName} not found. Run \"composer require {$vendorName}\" first.");

      return 1;
    }

    $composerJson = json_decode(File::get("{$vendorPath}/composer.json"));

    if ($composerJson->type !== 'larasar-module') {
      $this->error("{$vendorName} is not a Larasar module");

      return 1;
    }

    $name = Str::beforeLast(Str::after($vendorName, '/'), '-module');
    $studlyName = Str::studly($name);
    $moduleBasePath = larasar_module_path($studlyName);

    if (File::exists($moduleBasePath) && !$this->option('force')) {
      $this->error('Module already exists');

      return 1;
    }

    File::copyDirectory(base_path('vendor/' . $vendorName), $moduleBasePath);

    $this->info("Publishd module {$studlyName}");

    return 0;
  }
}
