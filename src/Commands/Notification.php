<?php

namespace Larasar\Module\Commands;

use Illuminate\Foundation\Console\NotificationMakeCommand;
use Larasar\Module\Commands\Traits\Module;

class Notification extends NotificationMakeCommand
{
  use Module;

  protected $name = 'module:notification';
}
