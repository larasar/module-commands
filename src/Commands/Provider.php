<?php

namespace Larasar\Module\Commands;

use Larasar\Module\Commands\Traits\Module;
use Illuminate\Foundation\Console\ProviderMakeCommand;

class Provider extends ProviderMakeCommand
{
  use Module;

  protected $name = 'module:provider';
}
