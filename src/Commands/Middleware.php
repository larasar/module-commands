<?php

namespace Larasar\Module\Commands;

use Larasar\Module\Commands\Traits\Module;
use Illuminate\Routing\Console\MiddlewareMakeCommand;

class Middleware extends MiddlewareMakeCommand
{
  use Module;

  protected $name = 'module:middleware';
}
