<?php

namespace Larasar\Module\Commands;

use Larasar\Module\Commands\Traits\Module;
use Illuminate\Foundation\Console\ConsoleMakeCommand;

class Command extends ConsoleMakeCommand
{
  use Module;

  protected $name = 'module:command';
}
