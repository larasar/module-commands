<?php

namespace Larasar\Module\Commands;

use Illuminate\Foundation\Console\ObserverMakeCommand;
use Larasar\Module\Commands\Traits\Module;

class Observer extends ObserverMakeCommand
{
  use Module;

  protected $name = 'module:observer';
}
