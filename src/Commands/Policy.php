<?php

namespace Larasar\Module\Commands;

use Larasar\Module\Commands\Traits\Module;
use Illuminate\Foundation\Console\PolicyMakeCommand;

class Policy extends PolicyMakeCommand
{
  use Module;

  protected $name = 'module:policy';
}
