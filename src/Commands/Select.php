<?php

namespace Larasar\Module\Commands;

use Larasar\Module\Commands\Traits\Module;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Cache;

class Select extends Command
{
  use Module;

  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'module:select { name : The module name }';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Selects the module under which other commands are scoped';

  /**
   * Execute the console command.
   *
   * @return int
   */
  public function handle()
  {
    $moduleName = $this->argument('name');
    $studlyName = Str::studly($moduleName);

    if (!$this->disk()->exists($studlyName)) {
      return $this->error(sprintf('%s module does not exist. Create it with "php artisan make:module %s"', $studlyName, $moduleName));
    }

    Cache::set($this->getCacheKey(), $studlyName);

    $this->call('module:current');

    return 0;
  }
}
