<?php

namespace Larasar\Module\Commands;

use Larasar\Module\Commands\Traits\Module;
use Illuminate\Foundation\Console\ModelMakeCommand;
use Illuminate\Support\Str;

class Model extends ModelMakeCommand
{
  use Module;

  protected $name = 'module:model';

  protected function getDefaultNamespace($rootNamespace)
  {
    return $rootNamespace . '\\Models';
  }

  protected function createFactory()
  {
    $factory = Str::studly($this->argument('name'));

    $this->call('module:factory', [
      'name' => "{$factory}Factory",
      '--model' => $this->qualifyClass($this->getNameInput()),
    ]);
  }

  protected function createMigration()
  {
    $table = Str::snake(Str::pluralStudly(class_basename($this->argument('name'))));

    if ($this->option('pivot')) {
      $table = Str::singular($table);
    }

    $this->call('module:migration', [
      'name' => "create_{$table}_table",
      '--create' => $table,
    ]);
  }

  protected function createSeeder()
  {
    $seeder = Str::studly(class_basename($this->argument('name')));

    $this->call('module:seeder', [
      'name' => "{$seeder}Seeder",
    ]);
  }

  protected function createController()
  {
    $controller = Str::studly(class_basename($this->argument('name')));

    $modelName = $this->qualifyClass($this->getNameInput());

    $this->call('module:controller', array_filter([
      'name' => "{$controller}Controller",
      '--model' => $this->option('resource') || $this->option('api') ? $modelName : null,
      '--api' => $this->option('api'),
    ]));
  }

  protected function createPolicy()
  {
    $policy = Str::studly(class_basename($this->argument('name')));

    $this->call('module:policy', [
      'name' => "{$policy}Policy",
      '--model' => $this->qualifyClass($this->getNameInput()),
    ]);
  }
}
