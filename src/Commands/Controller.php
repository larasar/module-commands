<?php

namespace Larasar\Module\Commands;

use Larasar\Module\Commands\Traits\Module;
use Illuminate\Routing\Console\ControllerMakeCommand;

class Controller extends ControllerMakeCommand
{
  use Module;

  protected $name = 'module:controller';
}
