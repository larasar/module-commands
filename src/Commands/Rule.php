<?php

namespace Larasar\Module\Commands;

use Illuminate\Foundation\Console\RuleMakeCommand;
use Larasar\Module\Commands\Traits\Module;

class Rule extends RuleMakeCommand
{
  use Module;

  protected $name = 'module:rule';
}
