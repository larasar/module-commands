<?php

namespace Larasar\Module\Commands;

use Illuminate\Foundation\Console\TestMakeCommand;
use Larasar\Module\Commands\Traits\Module;

class Test extends TestMakeCommand
{
  use Module;

  protected $name = 'module:test';

  protected function getDefaultNamespace($rootNamespace)
  {
    if ($this->option('unit')) {
      return $rootNamespace . '\Tests\Unit';
    } else {
      return $rootNamespace . '\Tests\Feature';
    }
  }
}
