<?php

namespace Larasar\Module\Commands;

use Larasar\Module\MigrationCreator;
use Larasar\Module\Commands\Traits\Module;
use Illuminate\Database\Console\Migrations\MigrateMakeCommand;
use Illuminate\Support\Composer;

class Migration extends MigrateMakeCommand
{
  use Module;

  protected $signature = 'module:migration {name : The name of the migration}
        {--create= : The table to be created}
        {--table= : The table to migrate}
        {--fullpath : Output the full path of the migration}';

  public function __construct(MigrationCreator $creator, Composer $composer)
  {
    parent::__construct($creator, $composer);
  }

  protected function getMigrationPath()
  {
    return $this->getRootPath('/Database/migrations');
  }
}
