<?php

namespace Larasar\Module;

use Composer\Installer\PackageEvent;
use Illuminate\Support\Str;

class Installer
{
  public static function postPackageInstall(PackageEvent $event)
  {
    $installedPackage = $event->getOperation()->getPackage();

    if ($installedPackage->getType() !== 'larasar-module') {
      return;
    }

    $name = Str::beforeLast(Str::after($installedPackage->getName(), 'larasar/'), '-module');
    $studlyName = Str::studly($name);

    $vendorPath = dirname(dirname(dirname(dirname(__FILE__))));
    $basePath = dirname($vendorPath);

    if (!file_exists($basePath . '/module')) {
      mkdir($basePath) . '/module';
    }

    $moduleBasePath = $basePath . "/module/$studlyName";

    if (file_exists($moduleBasePath)) {
      return;
    }

    $from = $vendorPath . '/' . $installedPackage->getName();

    exec("cp -r $from $moduleBasePath");
  }
}
