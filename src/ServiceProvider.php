<?php

namespace Larasar\Module;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Larasar\Module\Commands\Cast;
use Larasar\Module\Commands\Channel;
use Larasar\Module\Commands\Command;
use Larasar\Module\Commands\Controller;
use Larasar\Module\Commands\Create;
use Larasar\Module\Commands\Current;
use Larasar\Module\Commands\Event;
use Larasar\Module\Commands\Exception;
use Larasar\Module\Commands\Factory;
use Larasar\Module\Commands\Job;
use Larasar\Module\Commands\Listener;
use Larasar\Module\Commands\Middleware;
use Larasar\Module\Commands\Migrate;
use Larasar\Module\Commands\Migration;
use Larasar\Module\Commands\Model;
use Larasar\Module\Commands\Notification;
use Larasar\Module\Commands\Observer;
use Larasar\Module\Commands\Policy;
use Larasar\Module\Commands\Provider;
use Larasar\Module\Commands\Publish;
use Larasar\Module\Commands\Request;
use Larasar\Module\Commands\Resource;
use Larasar\Module\Commands\Rule;
use Larasar\Module\Commands\Seed;
use Larasar\Module\Commands\Seeder;
use Larasar\Module\Commands\Select;
use Larasar\Module\Commands\Status;
use Larasar\Module\Commands\Test;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                Cast::class,
                Channel::class,
                Command::class,
                Controller::class,
                Create::class,
                Current::class,
                Event::class,
                Exception::class,
                Factory::class,
                Job::class,
                Listener::class,
                Middleware::class,
                Migrate::class,
                Migration::class,
                Model::class,
                Notification::class,
                Observer::class,
                Policy::class,
                Provider::class,
                Publish::class,
                Request::class,
                Resource::class,
                Rule::class,
                Seed::class,
                Seeder::class,
                Select::class,
                Status::class,
                Test::class,
            ]);
        }
    }
}
