<?php

namespace Larasar\Module;

use Illuminate\Database\Migrations\MigrationCreator as MigrationsMigrationCreator;
use Illuminate\Filesystem\Filesystem;

class MigrationCreator extends MigrationsMigrationCreator
{
  public function __construct(Filesystem $files, $customStubPath = null)
  {
    parent::__construct($files, $customStubPath);
  }
}
